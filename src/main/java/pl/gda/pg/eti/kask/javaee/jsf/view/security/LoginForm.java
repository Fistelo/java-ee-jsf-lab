package pl.gda.pg.eti.kask.javaee.jsf.view.security;

import static pl.gda.pg.eti.kask.javaee.jsf.view.security.RequestUtils.getRequest;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class LoginForm implements Serializable {
  @Getter
  @Setter
  String login;

  @Getter
  @Setter
  String password;

  public String login() throws IOException {
    try {
      HttpServletRequest request = getRequest();
      request.login(login, password);
      return "/list_accounts.xhtml?faces-redirect=true";

    } catch (ServletException ex) {
      ex.printStackTrace();
      return null;
    }
  }
}
