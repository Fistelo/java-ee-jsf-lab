package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities;

import static java.math.BigDecimal.ZERO;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Getter
@Setter
public class Account implements Serializable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Size(min = 4, max = 250)
  private String name;

  @NotNull
  @Size(min = 16, max = 24)
  @Column(unique = true)
  private String number;

  @Min(value = 0)
  private BigDecimal balance = ZERO;

  @OneToMany(cascade = ALL, mappedBy = "account")
  private List<Profile> profiles = new ArrayList<>();

  public Account(final String name, final String number, final BigDecimal balance) {
    this.name = name;
    this.number = number;
    this.balance = balance;
  }
}
