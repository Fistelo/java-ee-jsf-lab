package pl.gda.pg.eti.kask.javaee.jsf.view.converters;

import javax.enterprise.context.Dependent;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.AccountService;

@FacesConverter(forClass = Account.class, managed = true)
@Dependent
public class AccountConverter implements Converter<Account> {

  @Inject
  private AccountService accountService;

  @Override
  public Account getAsObject(FacesContext context, UIComponent component, String value) {
    Account entity = accountService.findAccount(Integer.parseInt(value));

    if (entity == null) {
      context.getExternalContext().setResponseStatus(HttpServletResponse.SC_NOT_FOUND);
      context.responseComplete();
    }
    return entity;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Account profile) {
    final Integer id = profile.getId();
    return id != null ? id.toString() : null;
  }

}
