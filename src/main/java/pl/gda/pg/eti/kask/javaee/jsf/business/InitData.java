package pl.gda.pg.eti.kask.javaee.jsf.business;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao.UserJpaDao;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Profile;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.User;
import pl.gda.pg.eti.kask.javaee.jsf.business.utils.CryptUtils;

@ApplicationScoped
public class InitData {

  @PersistenceContext EntityManager em;

  @Inject
  private UserJpaDao userDao;

  @Transactional
  public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
    Card card1 = new Card("123", Date.valueOf(LocalDate.now().plusDays(10)), "541234567890125");
    Card card2 = new Card("563", Date.valueOf(LocalDate.now().plusDays(15)), "4111111111111111");

    Account b1 = new Account("Prywatne", "72456823764239876", BigDecimal.valueOf(800));
    Account b2 = new Account("Firmowe", "190273981273998765", BigDecimal.valueOf(1234));


    Profile profile1 = new Profile("Andrzej", "Strzelba", "01746283746");
    Profile profile2 = new Profile("Boguslaw", "Strzelba", "01744444611");

    User admin = new User("admin", CryptUtils.sha256("admin"), asList(Role.OWNER, Role.USER));
    User user = new User("user", CryptUtils.sha256("user"), singletonList(Role.USER));

    em.persist(admin);
    em.persist(user);

    profile1.setAccountMutually(b1);
    profile2.setAccountMutually(b2);

    profile1.addCardMutually(card1);
    profile2.addCardMutually(card2);

    em.persist(b1);
    em.persist(b2);
    em.persist(card1);
    em.persist(card2);
    em.persist(profile1);
    em.persist(profile2);

    em.flush();
  }
}
