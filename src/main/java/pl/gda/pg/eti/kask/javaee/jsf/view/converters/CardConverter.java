package pl.gda.pg.eti.kask.javaee.jsf.view.converters;

import javax.enterprise.context.Dependent;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.CardService;

@FacesConverter(forClass = Card.class, managed = true)
@Dependent
public class CardConverter implements Converter<Card> {
  @Inject
  private CardService cardService;

  @Override
  public Card getAsObject(FacesContext context, UIComponent component, String value) {
    Card entity = cardService.findCard(Integer.parseInt(value));

    if (entity == null) {
      context.getExternalContext().setResponseStatus(HttpServletResponse.SC_NOT_FOUND);
      context.responseComplete();
    }
    return entity;
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, Card profile) {
    final Integer id = profile.getId();
    return id != null ? id.toString() : null;
  }
}
