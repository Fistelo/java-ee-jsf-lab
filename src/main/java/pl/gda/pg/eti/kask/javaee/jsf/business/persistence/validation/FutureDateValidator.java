package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.validation;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FutureDateValidator implements ConstraintValidator<FutureDate, Date> {
  @Override
  public boolean isValid(Date value, ConstraintValidatorContext context) {
    if(value == null){
      return false;
    }
    final Date now = new Date(System.currentTimeMillis());
    return value.after(now);
  }
}
