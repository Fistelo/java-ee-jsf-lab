package pl.gda.pg.eti.kask.javaee.jsf.view;

import static pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role.OWNER;
import static pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role.USER;

import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Profile;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.ProfileService;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.login.AuthAspect;

@Named
@RequestScoped
public class ListProfiles implements Serializable {

  @Inject private ProfileService profileService;
  @Inject private AuthAspect authAspect;

  private Collection<Profile> profiles;

  @Getter
  @Setter
  private String filterParam = "";

  public Collection<Profile> getProfiles() {
    return profiles != null ? profiles : (profiles = profileService.findAllProfilesFiltered(filterParam));
  }

  public String removeProfile(Profile profile) {
    profileService.removeProfile(profile);
    return "list_profiles?faces-redirect=true";
  }

  public boolean canModifyProfiles(){
    return authAspect.hasRole(OWNER);
  }
  public boolean canShowProfiles(){
    return authAspect.hasRole(USER);
  }
}
