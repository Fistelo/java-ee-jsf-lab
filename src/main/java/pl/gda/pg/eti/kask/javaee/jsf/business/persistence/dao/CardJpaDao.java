package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;

@ApplicationScoped
public class CardJpaDao extends JpaDao<Card> {
  public CardJpaDao() {
    super(Card.class);
  }
}
