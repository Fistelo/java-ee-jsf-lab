package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao;

import javax.enterprise.context.ApplicationScoped;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;

@ApplicationScoped
public class AccountJpaDao extends JpaDao<Account> {
  public AccountJpaDao(){
    super(Account.class);
  }
}
