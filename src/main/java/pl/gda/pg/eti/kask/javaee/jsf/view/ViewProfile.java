package pl.gda.pg.eti.kask.javaee.jsf.view;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Profile;

@Named
@RequestScoped
public class ViewProfile implements Serializable {

  @Getter @Setter private Profile profile;
}
