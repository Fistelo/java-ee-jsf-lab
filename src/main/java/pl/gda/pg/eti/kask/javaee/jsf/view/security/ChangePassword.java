package pl.gda.pg.eti.kask.javaee.jsf.view.security;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.UserService;


@Named
@ViewScoped
public class ChangePassword implements Serializable {

  @Inject
  private UserService userService;
  @Getter @Setter
  private String oldPassword;
  @Getter @Setter
  private String newPassword;

  public String proceed(){
    userService.changePassword(oldPassword, newPassword);
    return "list_accounts?faces-redirect=true";
  }
}
