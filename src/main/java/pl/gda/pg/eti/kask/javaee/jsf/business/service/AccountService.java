package pl.gda.pg.eti.kask.javaee.jsf.business.service;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao.AccountJpaDao;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;

@ApplicationScoped
public class AccountService {
  @Inject
  private AccountJpaDao accountJpaDao;

  public Collection<Account> findAllAccounts() {
    return accountJpaDao.findAll();
  }

  public Account findAccount(int id) {
    return accountJpaDao.find(id);
  }

  @Transactional
  public void removeAccount(Account account) {
    accountJpaDao.remove(account);
  }

  @Transactional
  public Account saveAccount(Account account) {
    if (account.getId() == null) {
      accountJpaDao.add(account);
    } else {
      accountJpaDao.update(account);
    }
    return account;
  }
}
