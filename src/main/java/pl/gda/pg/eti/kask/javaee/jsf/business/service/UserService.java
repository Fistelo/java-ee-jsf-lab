package pl.gda.pg.eti.kask.javaee.jsf.business.service;

import static java.util.Objects.isNull;
import static pl.gda.pg.eti.kask.javaee.jsf.business.utils.CryptUtils.sha256;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao.UserJpaDao;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.User;

@ApplicationScoped
public class UserService implements Serializable {

  @Inject
  private UserJpaDao userJpaDao;

  public void changePassword(final String oldPassword, final String newPassword){
    final User currentUser = userJpaDao.findCurrentUser();
    if(!isNull(currentUser) && passwordsMatch(oldPassword, currentUser)){
      currentUser.setPassword(sha256(newPassword));
      userJpaDao.update(currentUser);
    }
  }

  private boolean passwordsMatch(final String password, final User user) {
    return user.getPassword().equals(sha256(password));
  }
}
