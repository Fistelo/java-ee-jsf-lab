package pl.gda.pg.eti.kask.javaee.jsf.view.security;

import static pl.gda.pg.eti.kask.javaee.jsf.view.security.RequestUtils.getRequest;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import pl.gda.pg.eti.kask.javaee.jsf.business.service.login.AuthAspect;

@Named
@RequestScoped
public class Authentication implements Serializable {
  @Inject
  private AuthAspect authAspect;

  public boolean isAuthenticated() {
    return authAspect.isAuthenticated();
  }

  public boolean isNotAuthenticated() {
    return !isAuthenticated();
  }

  public String logout() throws ServletException, IOException {
    HttpServletRequest request = getRequest();
    request.logout();
    request.getSession().invalidate();
    return "/login-form.xhtml?faces-redirect=true";
  }
}
