package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class User implements Serializable {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Column(nullable = false, unique = true)
  private String login;

  private String password;

  @ElementCollection
  private List<Role> roles = new ArrayList<>();

  public User(final String login, final String password, final List<Role> roles) {
    this.login = login;
    this.password = password;
    this.roles = roles;
  }
}
