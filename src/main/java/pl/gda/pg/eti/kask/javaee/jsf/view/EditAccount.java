package pl.gda.pg.eti.kask.javaee.jsf.view;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.AccountService;

@Named
@ViewScoped
public class EditAccount implements Serializable {

  @Inject private AccountService accountService;

  @Getter @Setter private Account account = new Account();

  public String saveAccount() {
    accountService.saveAccount(account);
    return "list_accounts?faces-redirect=true";
  }
}
