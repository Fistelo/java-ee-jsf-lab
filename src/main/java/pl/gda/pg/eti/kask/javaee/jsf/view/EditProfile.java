package pl.gda.pg.eti.kask.javaee.jsf.view;

import java.io.Serializable;
import java.util.Collection;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Profile;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.AccountService;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.CardService;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.ProfileService;

@Named
@ViewScoped
public class EditProfile implements Serializable {

  @Inject private ProfileService profileService;
  @Inject private CardService cardService;
  @Inject private AccountService accountService;

  @Getter @Setter private Profile profile = new Profile();

  public Collection<Card> getAllCards() {
    return cardService.findAllCards();
  }

  public Collection<Account> getAllAccounts() {
    return accountService.findAllAccounts();
  }

  public String saveProfile() {
    profileService.saveProfile(profile);
    return "list_profiles?faces-redirect=true";
  }
}
