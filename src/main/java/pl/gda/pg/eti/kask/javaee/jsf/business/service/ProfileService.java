package pl.gda.pg.eti.kask.javaee.jsf.business.service;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao.ProfileJpaDao;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Profile;

@ApplicationScoped
public class ProfileService implements Serializable {
  @Inject
  private ProfileJpaDao profileJpaDao;
  @Inject
  private CardService cardService;
  @Inject
  private AccountService accountService;

  public Collection<Profile> findAllProfiles() {
    return profileJpaDao.findAll();
  }

  public Collection<Profile> findAllProfilesFiltered(String filter) {
    return profileJpaDao.findAllFiltered(filter);
  }

  public Profile findProfile(int id) {
    return profileJpaDao.find(id);
  }

  @Transactional
  public void removeProfile(Profile profile) {
    profileJpaDao.remove(profile);
  }

  @Transactional
  public Profile saveProfile(Profile profile) {
    profile = addCards(profile);
    addProfile(profile);
    return profile;
  }

  private Profile addCards(Profile profile) {
    profile.setCards(
        profile.getCards()
            .stream()
            .map(card -> cardService.findCard(card.getId()))
            .collect(toList()));
    profile.setAccount(accountService.findAccount(profile.getAccount().getId()));
    return profile;
  }

  private Profile addProfile(Profile profile) {
    if (profile.getId() == null) {
      profileJpaDao.add(profile);
    } else {
      profile = profileJpaDao.update(profile);
    }
    return profile;
  }
}
