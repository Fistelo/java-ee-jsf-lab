package pl.gda.pg.eti.kask.javaee.jsf.view;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.CardService;

@Named
@ViewScoped
public class EditCard implements Serializable {

  @Inject private CardService cardService;

  @Getter @Setter private Card card = new Card();

  public String saveCard() {
    cardService.saveCard(card);
    return "list_cards?faces-redirect=true";
  }
}
