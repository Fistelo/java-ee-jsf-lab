package pl.gda.pg.eti.kask.javaee.jsf.view;

import static pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role.OWNER;
import static pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role.USER;

import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.CardService;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.login.AuthAspect;

@Named
@RequestScoped
public class ListCards implements Serializable {

  @Inject private CardService cardService;
  @Inject private AuthAspect authAspect;

  private Collection<Card> cards;

  public Collection<Card> getCards() {
    return cards != null ? cards : (cards = cardService.findAllCards());
  }

  public String removeCard(Card card) {
    cardService.removeCard(card);
    return "list_cards?faces-redirect=true";
  }

  public boolean canModifyCards(){
    return authAspect.hasRole(OWNER);
  }
  public boolean canShowCards(){
    return authAspect.hasRole(USER);
  }
}
