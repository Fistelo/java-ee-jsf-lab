package pl.gda.pg.eti.kask.javaee.jsf.view;

import static pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role.OWNER;
import static pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.security.Role.USER;

import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Account;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.AccountService;
import pl.gda.pg.eti.kask.javaee.jsf.business.service.login.AuthAspect;

@Named
@RequestScoped
public class ListAccounts implements Serializable {

  @Inject private AccountService accountService;
  @Inject private AuthAspect authAspect;

  private Collection<Account> accounts;

  public Collection<Account> getAccounts() {
    return accounts != null ? accounts : (accounts = accountService.findAllAccounts());
  }

  public String removeAccount(Account account) {
    accountService.removeAccount(account);
    return "list_accounts?faces-redirect=true";
  }

  public boolean canModifyAccounts(){
    return authAspect.hasRole(OWNER);
  }
  public boolean canShowAccount(){
    return authAspect.hasRole(USER);
  }
}
