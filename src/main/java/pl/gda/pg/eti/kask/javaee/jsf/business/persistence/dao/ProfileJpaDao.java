package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Profile;

@ApplicationScoped
public class ProfileJpaDao extends JpaDao<Profile> {
  private static final String FILTER_PARAM = "filter";

  public ProfileJpaDao() {
    super(Profile.class);
  }

  public List<Profile> findAllFiltered(String filter){
    return entityManager.createNamedQuery(Profile.Queries.FIND_ALL_FILTERED, Profile.class)
        .setParameter(FILTER_PARAM, filter)
        .getResultList();
  }
}