package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.CreditCardNumber;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.validation.FutureDate;

@Entity
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Getter
@Setter
public class Card implements Serializable {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Size(min = 3, max = 4)
  private String cvv;

  @Column(unique = true)
  @CreditCardNumber
  private String number;

  @FutureDate
  private Date expirationDate;

  @ManyToMany(cascade = ALL, mappedBy = "cards")
  private List<Profile> profiles = new ArrayList<>();

  public Card(String cvv, Date expirationDate, String number) {
    this.cvv = cvv;
    this.number = number;
    this.expirationDate = expirationDate;
  }
}
