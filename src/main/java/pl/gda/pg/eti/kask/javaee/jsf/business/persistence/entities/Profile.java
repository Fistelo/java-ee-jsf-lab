package pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Getter
@Setter
@NamedQueries({
    @NamedQuery(
        name = Profile.Queries.FIND_ALL_FILTERED,
        query = "select b from Profile b where b.lastName like CONCAT('%',:filter,'%')"
    )
})
public class Profile implements Serializable {
  @Id
  @GeneratedValue
  private Integer id;

  @Size(min = 4, max = 250)
  private String firstName;

  @Size(min = 4, max = 250)
  private String lastName;

  @Size(min = 11, max = 11)
  private String referenceId;

  @ManyToMany(cascade = { MERGE, REFRESH })
  private List<Card> cards = new ArrayList<>();

  @NotNull
  @ManyToOne(cascade = { MERGE, REFRESH })
  private Account account;

  public Profile(final String firstName, final String lastName, final String referenceId) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.referenceId = referenceId;
  }

  public void addCardMutually(Card card) {
    card.getProfiles().add(this);
    cards.add(card);
  }

  public void setAccountMutually(Account account) {
    account.getProfiles().add(this);
    this.account = account;
  }


  public static class Queries {
    public static final String FIND_ALL_FILTERED = "PROFILE_FIND_ALL_FILTERED";
  }
}
