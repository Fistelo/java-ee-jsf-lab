package pl.gda.pg.eti.kask.javaee.jsf.business.service;

import java.util.Collection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.dao.CardJpaDao;
import pl.gda.pg.eti.kask.javaee.jsf.business.persistence.entities.Card;

@ApplicationScoped
public class CardService {
  @Inject
  private CardJpaDao cardJpaDao;

  public Collection<Card> findAllCards() {
    return cardJpaDao.findAll();
  }

  public Card findCard(int id) {
    return cardJpaDao.find(id);
  }

  @Transactional
  public void removeCard(Card card) {
    cardJpaDao.remove(card);
  }

  @Transactional
  public Card saveCard(Card card) {
    if (card.getId() == null) {
      cardJpaDao.add(card);
    } else {
      cardJpaDao.update(card);
    }
    return card;
  }
}
